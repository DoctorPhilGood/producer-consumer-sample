package physiq

import org.junit.Assert.*
import org.junit.Test

class ProducerTest {
    @Test
    fun `Puts designated number of things on the stack`() {
        val myStack = Stack()
        produceStuff(myStack, 5)
//        Thread.sleep(10_000)
        assertEquals(6, myStack.size())
    }

    @Test
    fun `ends with a -1`() {
        val myStack = Stack()
        produceStuff(myStack, 5)
//        Thread.sleep(10_000)
        assertEquals(-1, myStack.pop())
    }

}