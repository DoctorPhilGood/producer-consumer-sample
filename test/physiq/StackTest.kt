package physiq

import org.junit.Assert.assertEquals
import org.junit.Test

class StackTest {

    @Test
    fun `push to stack`() {
        val myStack = Stack()
        myStack.push(5)
        assertEquals(5, myStack.data[0])

    }

    @Test
    fun `pop off stack`() {
        val myStack = Stack()
        myStack.push(5)
        assertEquals(5, myStack.pop())
    }

    @Test
    fun `size of stack`() {
        val myStack = Stack()
        assertEquals(0, myStack.size())
        myStack.push(5)
        assertEquals(1, myStack.size())

    }

}