package physiq

import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock

class Stack {

    val data: ArrayList<Int> = ArrayList()

    val theLock = ReentrantLock()

    fun push(value : Int) {
        theLock.withLock {
            data.add(value)
        }
    }

    fun pop(): Int {
        theLock.withLock {
            val valueToRemove = data[(data.size - 1)]
            data.remove(data.size - 1)
            return valueToRemove
        }
    }

    fun size(): Int {
        return theLock.withLock {
            data.size
        }
    }


}