package physiq

import java.util.*

fun produceStuff(mahStack: Stack, numToProduce: Int) {
    (1..numToProduce).forEach {
        val randomNum = Random(0).nextInt()
        mahStack.push(randomNum)
    }

    mahStack.push(-1)
}